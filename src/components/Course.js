import React, {useState} from 'react';
//useState returns 2 things: 1. Default state, 2. State changer
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function Course({course}) {
	
	const {name, description, price, start_date, end_date} = course;

	const [count, setCount] = useState(0);

	const [seats, setSeats] = useState(30);

	const [isOpen, setIsOpen] = useState(true);

	React.useEffect(() => {
		if(seats === 0)
			setIsOpen(false)
		}
	)

	function enroll(){
		setCount(count + 1);
		setSeats(seats - 1);
	}
	return (
	<Card>
		<Card.Body>	
			<Card.Title>{name}</Card.Title>
			<Card.Text>
				<span className="subtitle">Description:</span>
                <br />
                {description}
                <br />
                <span className="subtitle">Price: </span>
                &#8369;{price}
                <br />
                <span className="subtitle">Start Date: </span>
                {start_date}
                <br />
                <span className="subtitle">End Date: </span>
                {end_date}
                <br />
                <span className="subtitle">Enrollees: </span>
                {count}
                <br />
                <span className="subtitle">Seats: </span>
                {seats}
			</Card.Text>
			{ isOpen ?
			<Button variant="primary" onClick={enroll}>Enroll</Button>
			:
			<Button variant="primary" disabled>Enroll</Button>
			}
		</Card.Body>
	</Card>
	)
}

//check that the Course component is getting the correct prop types
Course.propTypes = {
	//shape() is used to check that a prop object conforms to a specific "shape"
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		start_date: PropTypes.string.isRequired,
		end_date: PropTypes.string.isRequired

	})
}