import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
//consume UserContext and obtain unsetUser and setUser from it

const { unsetUser, setUser } = useContext(UserContext);
//invoke unsetUser()
unsetUser();

//set user as an object with email: null
setUser({email: null})


return(
	<Redirect to='/login' />
	)
}