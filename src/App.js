import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CreateCourse from './pages/CreateCourse';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import { UserProvider } from './UserContext'

import './App.css';

function App() {

const [user, setUser] = useState({
	email: localStorage.getItem('email'),
	isAdmin: localStorage.getItem('isAdmin') === true
});

//function for clearing local storage upon logout
const unsetUser = () => {
	localStorage.clear();
}

  return (
  	<UserProvider value={{user, setUser, unsetUser}}>
	    <Router>
	      <NavBar />
	      <Container>
	      <Switch>
	      <Route exact path= "/" component={Home} />
	      <Route exact path= "/courses" component={Courses} />
	      <Route exact path= "/courses/create" component={CreateCourse} />
	      <Route exact path= "/login" component={Login} />
	      <Route exact path= "/register" component={Register} />
	      <Route exact path= "/logout" component={Logout} />
	      <Route component={Error} />
	      </Switch>
	      </Container>
	    </Router>
	</UserProvider>
  );
}

export default App;
