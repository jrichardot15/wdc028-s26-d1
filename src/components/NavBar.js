import React, {useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';

import UserContext from '../UserContext'

export default function NavBar() {

  const { user } = useContext(UserContext)

	return (    
	<Navbar bg="light" expand="lg">
      <Navbar.Brand href="/home">React-Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
          <Nav.Link href="/courses">Courses</Nav.Link>

          {(user.email !== null)
            ? (user.isAdmin === true)
            ? <><Nav.Link href="courses/create">Add Course</Nav.Link><Nav.Link href="/logout">Log Out</Nav.Link></>
              :<Nav.Link href="/logout">Log Out</Nav.Link>
            : <><Nav.Link href="/login">Log In</Nav.Link><Nav.Link href="/register">Register</Nav.Link></>
          }
          
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    )
}